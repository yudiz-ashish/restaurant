import React from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import './App.css'
import CheckOut from './View/pages/checkout'
import Home from './View/pages/Home'
function App () {
  return (
    <>
      <div className="app_container">
        <div className="card">
          <BrowserRouter>
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/checkout" element={<CheckOut />} />
            </Routes>
          </BrowserRouter>
        </div>
      </div>
    </>
  )
}

export default App
