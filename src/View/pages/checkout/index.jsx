import React from 'react'
import Header from '../../../shared/components/header'
import './style.css'
import { GrPrevious } from 'react-icons/gr'
import { useSelector } from 'react-redux'
// import { GoThumbsup } from 'react-icons/go'
import { HiDotsHorizontal } from 'react-icons/hi'
import { useNavigate } from 'react-router-dom'

function CheckOut () {
  const navigate = useNavigate()
  const tokenNumber = Math.floor(Math.random() * 100) + 1
  const getOrderData = useSelector((state) => state.allCartData.cartData)
  console.log('getOrderDate :>> ', getOrderData[0].orderName.id)
  return (
    <>
    <div>
      <div className='checkout_header'>
      <h1><GrPrevious onClick={() => navigate('/')}/></h1>
      <h3>Checkout</h3>
      <h1><HiDotsHorizontal/></h1>
      </div>
      <Header />
      <div className="check_container">
        <div className="check_details">
          <h1 className="check_title">{getOrderData.map((data) => data.orderName.name)}</h1>
          <div className="check_item">
            <h3>2 X order</h3>
            <h3>5</h3>
          </div>
        </div>
        <div className="check_note">
          <label htmlFor="Add_note">Add note</label>
          <textarea name="" id="" cols="3" rows="3"></textarea>
        </div>
        <span className='order_underline'></span>
        <div className="order_number">
          <h1>Table number </h1>
          <h1 className="token_number">{tokenNumber}</h1>
        </div>
        {/* <div className="confirm_order">
            <div className='thumb'>
                <h1>Confirm_order</h1>
            <GoThumbsup className='comfirm_thumb'/>
            </div>
          <h2>
            By placing this order you agree that you are present in Kings Arms
            and Over 18 years old
          </h2>
          <div className="order_button">
          <button className='cancel_order'> cancel</button>
            <button className='place_order'>PLACE ORDER</button>
          </div>
        </div> */}
      </div>
    </div>
    </>
  )
}

export default CheckOut
