import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import Header from '../../../shared/components/header'
import { setProducts, selectProducts, orderCart } from '../../../shared/redux/action'
import { getAllData, getAllProduct } from '../../../shared/constant'
import { BiDollar } from 'react-icons/bi'
import { TbCurrencyPound } from 'react-icons/tb'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import './style.css'
import { useNavigate } from 'react-router-dom'

function Home () {
  const categoryItem = useSelector((state) => state.allProducts.usersData)
  const productsItem = useSelector((state) => state.allItem)
  const getOrderData = useSelector((state) => state.allCartData.cartData)
  const [subCategory, setSubCategory] = useState([])
  const [subItem, setSubItem] = useState([])
  const [toggle, setToggle] = useState(false)
  const [variant, setVariant] = useState([])
  const [extras, setExtras] = useState([])
  const [orderName, setOrderName] = useState([])
  const [count, setCount] = useState(0)
  const [size, setSize] = useState([])
  const [select, setSelect] = useState([])

  const dispatch = useDispatch()
  const navigate = useNavigate()
  // get all category
  const getCategoryData = async () => {
    const response = await getAllData()
    dispatch(setProducts(response.data))
  }

  // get all product
  const getProductData = async () => {
    const response = await getAllProduct()
    dispatch(selectProducts(response.data))
  }

  // handleClick find subCategory
  const handleClick = (e, data) => {
    setToggle(false)
    console.log('data', data)
    const id = data.id
    const subCategory = categoryItem.filter((product) => id === product.parent)
    console.log('subC', subCategory)
    setSubCategory(subCategory)
    dispatch(setVariant(subCategory))
    e.preventDefault()
  }

  // handle subcategory
  const handleSubCategory = (e, subData) => {
    e.preventDefault()
    setToggle(false)
    const id = subData.id
    const setItem = productsItem.filter(
      (filterData) => id === filterData.parentId
    )
    setSubItem(setItem)
  }

  const clickItem = (e, data, title) => {
    setOrderName(data)
    const itemVariants = data.variants
    const extras = data.extras
    setVariant(itemVariants)
    setExtras(extras)
    setToggle(true)
    e.preventDefault()
  }

  const increment = () => {
    setCount((prevState) => prevState + 1)
  }
  const decrement = () => {
    if (count <= 0) {
      toast.error('minimum Order is one')
    } else {
      setCount((prevState) => prevState - 1)
    }
  }
  const close = () => {
    setToggle(false)
  }
  const addOrder = (e, size, select, count, orderName) => {
    setToggle(false)
    dispatch(orderCart(size, select, count, orderName))
    // navigate('/checkout')
  }
  console.log('getOrderData', getOrderData)

  const getVariant = (e, data, index) => {
    const { checked } = e.target
    if (checked) {
      setSize([data])
    }
  }
  console.log('size', size)
  const getExtras = (e, data, index) => {
    const id = index
    const { checked } = e.target
    if (checked) {
      setSelect((prevState) => [...prevState, data])
    } else {
      const el = select.filter((data, index) => { return id !== index })
      setSelect(el)
    }
  }
  console.log('select', select)
  useEffect(() => {
    getCategoryData()
    getProductData()
  }, [])
  return (
    <>
      <Header />
      <div className="food_menu">
        <div className="food_category">
          {categoryItem.map((data, index) => {
            // console.log('data :>> ', data)
            return (
              <React.Fragment key={index}>
                {data.parent === null
                  ? (
                  <button onClick={(e) => handleClick(e, data)}>
                   {data.name}
                  </button>
                    )
                  : (
                      ''
                    )}
              </React.Fragment>
            )
          })}
        </div>
      </div>
      <div className="subCategory">
        {subCategory
          ? subCategory?.map((subCategory, index) => {
            // console.log('subCategory :>> ', subCategory)
            return (
                <React.Fragment key={index}>
                  {
                    <button onClick={(e) => handleSubCategory(e, subCategory)}>
                      {subCategory.name}
                    </button>
                  }
                </React.Fragment>
            )
          })
          : 'not order'}
      </div>
      {subItem
        ? subItem?.map((data, index) => {
          return (
              <React.Fragment key={index}>
                <div className="item_details">
                  <div
                    className="item_name"
                    onClick={(e) => clickItem(e, data)}
                  >
                    <h3>{data.name}</h3>
                    <p>{data.description}</p>
                  </div>
                  <div className="item_price">
                    <BiDollar className="dollar" />
                    {data.price}
                  </div>
                </div>
              </React.Fragment>
          )
        })
        : 'Not Item'}
      <div className="toggle">
        {
        toggle
          ? (
          <>
            <div className="orderRemember">
              <div className="orderName">
                <div>
                  <h1>{orderName.name}</h1>
                </div>
                <div>
                  <button onClick={() => close()}>X</button>
                </div>
              </div>
              <span className="underline"></span>
              <div className="variants">
                <h1>size</h1>
                {
                variant
                  ? variant?.map((data, index) => {
                    // console.log('data.name >> ', data)
                    return (
                        <React.Fragment key={index}>
                          <div
                            className="inner-content"
                          >
                            <input type="radio" name="pint" value={data} id="index" onClick={(e) => getVariant(e, data, index)} />
                            <span>{data.name} </span>
                            <span>
                              <TbCurrencyPound />
                              {data.price}
                            </span>
                          </div>
                        </React.Fragment>
                    )
                  })
                  : 'no Variant'
                  }
              </div>
              <div className="extra_item">
                <h1>Select Options</h1>
                {extras
                  ? extras?.map((data, index) => {
                    return (
                        <React.Fragment key={index}>
                          <div id="extras">
                            <div>
                              <span>{data.name}</span>{' '}
                              <span>
                                (<TbCurrencyPound />
                                {data.price})
                              </span>
                            </div>
                            <div className="checkbox-div">
                              <input
                                type="checkbox"
                                value={data}
                                onChange={(e) => getExtras(e, data, index)}
                              />
                            </div>
                          </div>
                        </React.Fragment>
                    )
                  }
                  )
                  : 'no Extra'
                   }

              </div>
              <div className="item_count">
                <button onClick={() => decrement()}>-</button>
                <input
                  type="text"
                  name=""
                  id=""
                  value={count}
                  onChange={(e) =>
                    console.log('e.target.value', e.target.value)
                  }
                />
                <button onClick={() => increment()}>+</button>
              </div>
              <div className="addOrder">
                <button onClick={(e) => addOrder(e, size, select, count, orderName)}>ADD TO ORDER</button>
              </div>
            </div>
          </>
            )
          : (
              ''
            )}
      </div>
      <div className="item_footer" onClick={() => navigate('/checkout')}>
      <ToastContainer />
        <h3>view basket</h3>
        <p> item</p>
        <p>{orderName.name}</p>
      </div>
    </>
  )
}

export default Home
