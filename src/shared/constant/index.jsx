import axios from 'axios'

const USER = {
  SET_PRODUCT: 'SET_PRODUCT',
  SELECTED_PRODUCT: 'SELECTED_PRODUCT',
  REMOVE_SELECTED_PRODUCT: 'REMOVE_SELECTED_PRODUCT',
  SET_VARIANT: 'SET_VERIANT',
  SET_EXTRAS: 'SET_EXTRAS',
  GET_ORDER: 'GET_ORDER',
  GET_SUM: 'GET_SUM',
  GET_CART_DATA: 'GET_CART_DATA'
}

const product = axios.create({
  baseURL: 'https://6364aace7b209ece0f4adfbb.mockapi.io'
})

// https://6364aace7b209ece0f4adfbb.mockapi.io/category
const category = axios.create({
  baseURL: 'https://6364aace7b209ece0f4adfbb.mockapi.io'
})

export async function getAllData () {
  try {
    const response = await category.get('/category')
    if (response.status === 200) {
      return response
    } else {
      return { success: false }
    }
  } catch {
    console.log('Something went wrong')
  }
}

export async function getAllProduct () {
  try {
    const response = await product.get('/product')
    if (response.status === 200) {
      return response
    } else {
      return { success: false }
    }
  } catch {
    console.log('Something went wrong')
  }
}
export default USER
