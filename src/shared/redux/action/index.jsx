import USER from '../../constant'

export const setProducts = (products) => {
  // console.log('products :>> ', products)
  return {
    type: USER.SET_PRODUCT,
    payload: products
  }
}

export const selectProducts = (products) => {
  return {
    type: USER.SELECTED_PRODUCT,
    payload: products
  }
}

export const setVariant = (product) => {
  return {
    type: USER.SET_VARIANT,
    payload: product
  }
}
export const setExtras = (product) => {
  return {
    type: USER.SET_EXTRAS,
    payload: product
  }
}
export const orderData = (order) => {
  console.log('order  :>> ', order)
  return {
    type: USER.GET_ORDER,
    payload: order
  }
}
export const totalSum = (sum) => {
  return {
    type: USER.GET_SUM,
    payload: sum
  }
}

export const orderCart = (size, select, count, orderName) => {
  return {
    type: USER.GET_CART_DATA,
    payload: { size, select, count, orderName }
  }
}
