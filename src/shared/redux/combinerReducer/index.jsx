import { combineReducers } from 'redux'
import { extraReducer, getOrder, productReducer, reducer, variantReducer, cartReducer } from '../reducer'

const rootReducer = combineReducers({
  allProducts: reducer,
  allItem: productReducer,
  allVariant: variantReducer,
  allExtra: extraReducer,
  allOrder: getOrder,
  allCartData: cartReducer
})

export default rootReducer
