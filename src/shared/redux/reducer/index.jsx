import USER from '../../constant'

const initialState = {
  usersData: []
}

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case USER.SET_PRODUCT:
      // console.log('action', action)
      return {
        ...state, usersData: action.payload
      }
    default:
      return state
  }
}

export const productReducer = (state = [], action) => {
  switch (action.type) {
    case USER.SELECTED_PRODUCT:
      return [
        ...action.payload
      ]
    default:
      return state
  }
}

export const variantReducer = (state = [], action) => {
  switch (action.type) {
    case USER.SET_VARIANT:
      console.log('selected :>> ', action)
      return [
        ...action.payload
      ]
    default:
      return state
  }
}

export const extraReducer = (state = [], action) => {
  switch (action.type) {
    case USER.SET_EXTRAS:
      console.log('selected :>> ', action)
      return [
        ...action.payload
      ]
    default:
      return state
  }
}

export const getOrder = (state = [], action) => {
  switch (action.type) {
    case USER.GET_ORDER:
      return [
        ...action.payload
      ]
    default:
      return state
  }
}

export const getSum = (state = 0, action) => {
  switch (action.type) {
    case USER.GET_SUM:
      console.log('selected :>> ', action.payload)
      return [
        ...action.payload
      ]
    default:
      return state
  }
}

const initialCart = {
  cartData: []
}
export const cartReducer = (state = initialCart, action) => {
  switch (action.type) {
    case USER.GET_CART_DATA:
      console.log('selected :>> ', action.payload)
      return {
        ...state,
        cartData: [...state.cartData, action.payload]
      }
    default:
      return state
  }
}
